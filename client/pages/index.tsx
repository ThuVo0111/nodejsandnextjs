import React, { useState, useEffect } from 'react';
import {io} from "socket.io-client";
const socket = io.connect('http://localhost:8080');
import Link from 'next/link';
import Footer from '@/components/footer';

function Index() {
  const [brand, setBrand] = useState([]);
  const [message, setMessage] = useState([]);
  const [messageReceived, setMessageReceived] = useState([]);


  useEffect(() => {
    fetch("http://localhost:8080/api/home")
      .then(response => response.json())
      .then(data => {
        setBrand(data.brand);
      });
  }, []);

  useEffect(()=>{
    const receiveMessageHandler = (data) => {
      setMessageReceived((prev) => [...prev, data.message]);
    };
  
    socket.on('receive_message', receiveMessageHandler);
    return () => {
      socket.off('receive_message', receiveMessageHandler);
    };
  },[socket])

  const sendMessage =()=>{
    socket.emit("send_message",{ message:message});
    setMessage('');
  }

  return (
    <>
     <Link href="/about">
        Go to about
     </Link>
      <div>Danh sách sản phẩm</div>
      {brand.map((brand) => (
        <div key={brand.id}>{brand.name}</div>
      ))}
      <div>
      <div style={{marginTop: '50px'}}>Chat ngay tư vấn sản phẩm</div>
        <div>
          <input type="text"  style={{ border: '1px solid #000' }} onChange={(e)=>setMessage(e.target.value)}/>
          <button style={{ border: '1px solid #000'}} onClick={sendMessage}>Gửi</button>
        </div>
        <div>Hiển thị cuộc trò chuyện</div>
        {messageReceived.map((msg, index) => (
            <div key={index}>{msg}</div>))}
      </div>
      <Footer/>
    </>
  );
}

export default Index;