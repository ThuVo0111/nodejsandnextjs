const express = require('express');
const app = express();
const cors = require('cors');
const http =require('http');
const PORT = 8080;
const {Server}=require('socket.io');
app.use(cors());
app.use(express.json());
const server = http.createServer(app);
const io = new Server(server,{
    cors: {
        origin: "http://localhost:3000"
    },
});

let brandList = [
    {
        id: 1,
        name: 'Iphone',
    },
    {
        id: 2,
        name: 'Samsung',
    },
    {
        id: 3,
        name: 'Nokia',
    },
];

io.on("connection", (socket) => {
    console.log(`user connected: ${socket.id}`);
    socket.on("send_message", (datas)=>{
      io.emit("receive_message", datas);
    })
});

app.get('/api/home', (req, res) => {
    res.json({
        brand: brandList,
    });
});

app.post('/api/home', (req, res) => {
    const newBrand = req.body;
    brandList.push(newBrand);

    res.json({
        success: true,
        message: 'Brand added successfully',
        brand: newBrand,
    });
});

server.listen(PORT, () => {
    console.log('Server started on port', PORT);
});